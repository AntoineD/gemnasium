{
  "version": "14.0.4",
  "vulnerabilities": [
    {
      "id": "5776be8fe8282d0269e981cb76f5dc8a1cf143f868af14227a58ad6272e277f6",
      "category": "dependency_scanning",
      "name": "SQL Injection",
      "message": "SQL Injection in Django",
      "description": "Due to an error in shallow key transformation, key and index lookups for `django.contrib.postgres.fields.JSONField`, and key lookups for `django.contrib.postgres.fields.HStoreField`, were subject to SQL injection. This could, for example, be exploited via crafted use of `OR 1=1` in a key or index name to return all records, using a suitably crafted dictionary, with dictionary expansion, as the `**kwargs` passed to the `QuerySet.filter()` function.",
      "cve": "Pipfile:Django:gemnasium:1c876f07-617f-4ba9-8ea3-a871d14bcde2",
      "severity": "Critical",
      "solution": "Upgrade to versions 1.11.23, 2.1.11, 2.2.4 or above.",
      "scanner": {
        "id": "gemnasium",
        "name": "Gemnasium"
      },
      "location": {
        "file": "Pipfile",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-1c876f07-617f-4ba9-8ea3-a871d14bcde2",
          "value": "1c876f07-617f-4ba9-8ea3-a871d14bcde2",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2019-14234.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2019-14234",
          "value": "CVE-2019-14234",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-14234"
        }
      ],
      "links": [
        {
          "url": "https://docs.djangoproject.com/en/dev/releases/security/"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-14234"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2019/aug/01/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "name": "Vulnerable Package",
          "type": "text",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "463d85095c09b1eea6864cb45925283b6106ad329b966dd451dae42bbc641b95",
      "category": "dependency_scanning",
      "name": "Weak Password Recovery Mechanism for Forgotten Password",
      "message": "Weak Password Recovery Mechanism for Forgotten Password in Django",
      "description": "Django allows account takeover. A suitably crafted email address (that is equal to an existing user's email address after case transformation of Unicode characters) would allow an attacker to be sent a password reset token for the matched user account. (One mitigation in the new releases is to send password reset tokens only to the registered user email address.)",
      "cve": "Pipfile:Django:gemnasium:e1db52dc-7f84-4281-8b82-6a64da18a721",
      "severity": "Critical",
      "solution": "Upgrade to versions 1.11.27, 2.2.9, 3.0.1 or above.",
      "scanner": {
        "id": "gemnasium",
        "name": "Gemnasium"
      },
      "location": {
        "file": "Pipfile",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-e1db52dc-7f84-4281-8b82-6a64da18a721",
          "value": "e1db52dc-7f84-4281-8b82-6a64da18a721",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2019-19844.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2019-19844",
          "value": "CVE-2019-19844",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-19844"
        }
      ],
      "links": [
        {
          "url": "https://docs.djangoproject.com/en/dev/releases/security/"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-19844"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2019/dec/18/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "name": "Vulnerable Package",
          "type": "text",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "a0f52e33a4dc8d0b280548c18509c4aef2be6315ea401c1263f43f14a2d394e0",
      "category": "dependency_scanning",
      "name": "SQL Injection",
      "message": "SQL Injection in Django",
      "description": "Django allows SQL Injection if untrusted data is used as a delimiter (e.g., in Django applications that offer downloads of data as a series of rows with a user-specified column delimiter). By passing a suitably crafted delimiter to a `contrib.postgres.aggregates.StringAgg` instance, it was possible to break escaping and inject malicious SQL.",
      "cve": "Pipfile:Django:gemnasium:ea400310-29db-4c48-8236-42d462d1de1c",
      "severity": "Critical",
      "solution": "Upgrade to versions 1.11.28, 2.2.10, 3.0.3 or above.",
      "scanner": {
        "id": "gemnasium",
        "name": "Gemnasium"
      },
      "location": {
        "file": "Pipfile",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-ea400310-29db-4c48-8236-42d462d1de1c",
          "value": "ea400310-29db-4c48-8236-42d462d1de1c",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2020-7471.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2020-7471",
          "value": "CVE-2020-7471",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-7471"
        }
      ],
      "links": [
        {
          "url": "https://docs.djangoproject.com/en/3.0/releases/security/"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2020-7471"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2020/feb/03/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "name": "Vulnerable Package",
          "type": "text",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "b604a615ea1d17629268ce7d7c04ad26a171655d8933c0919bf8d8a215a84d26",
      "category": "dependency_scanning",
      "name": "Information exposure in HTTP headers",
      "message": "Information exposure in HTTP headers in requests",
      "description": "The Requests package for Python sends an HTTP Authorization header to an HTTP URI upon receiving a same-hostname https-to-http redirect, which makes it easier for remote attackers to discover credentials by sniffing the network.",
      "cve": "Pipfile:requests:gemnasium:2d0dd26c-c781-40e1-b126-7f20b21de048",
      "severity": "Critical",
      "solution": "Upgrade to version 2.20.0 or above.",
      "scanner": {
        "id": "gemnasium",
        "name": "Gemnasium"
      },
      "location": {
        "file": "Pipfile",
        "dependency": {
          "package": {
            "name": "requests"
          },
          "version": "2.5.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-2d0dd26c-c781-40e1-b126-7f20b21de048",
          "value": "2d0dd26c-c781-40e1-b126-7f20b21de048",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/requests/CVE-2018-18074.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2018-18074",
          "value": "CVE-2018-18074",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-18074"
        }
      ],
      "links": [
        {
          "url": "http://cwe.mitre.org/data/definitions/255.html"
        },
        {
          "url": "http://docs.python-requests.org/en/master/community/updates/#release-and-version-history"
        },
        {
          "url": "https://bugs.debian.org/910766"
        },
        {
          "url": "https://github.com/requests/requests/commit/c45d7c49ea75133e52ab22a8e9e13173938e36ff"
        },
        {
          "url": "https://github.com/requests/requests/issues/4716"
        },
        {
          "url": "https://github.com/requests/requests/pull/4718"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2018-18074"
        },
        {
          "url": "https://usn.ubuntu.com/3790-1/"
        },
        {
          "url": "https://usn.ubuntu.com/3790-2/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "name": "Vulnerable Package",
          "type": "text",
          "value": "requests:2.5.3"
        }
      }
    },
    {
      "id": "199a9ba3fc04f2e61d99acceed051664ab23f6076673737a68e8d5f7833564fe",
      "category": "dependency_scanning",
      "name": "Improper Input Validation",
      "message": "Improper Input Validation in Django",
      "description": "A call to the methods `chars()` or `words() in `django.utils.text.Truncator` with the argument `html=True` evaluates certain inputs extremely slowly due to a catastrophic backtracking vulnerability in a regular expression. The `chars()` and `words()` methods are used to implement the `truncatechars_html` and `truncatewords_html` template filters, which were thus vulnerable.",
      "cve": "Pipfile:Django:gemnasium:3fee86b0-d8c1-4c65-9242-7607562190ae",
      "severity": "High",
      "solution": "Upgrade to versions 1.11.23, 2.1.11, 2.2.4 or above.",
      "scanner": {
        "id": "gemnasium",
        "name": "Gemnasium"
      },
      "location": {
        "file": "Pipfile",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-3fee86b0-d8c1-4c65-9242-7607562190ae",
          "value": "3fee86b0-d8c1-4c65-9242-7607562190ae",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2019-14232.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2019-14232",
          "value": "CVE-2019-14232",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-14232"
        }
      ],
      "links": [
        {
          "url": "https://docs.djangoproject.com/en/dev/releases/security/"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-14232"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2019/aug/01/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "name": "Vulnerable Package",
          "type": "text",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "8d347c8bea0215190b4ca13224167ab161574dc7a58288c13d3e29c224a4d890",
      "category": "dependency_scanning",
      "name": "Denial-of-service",
      "message": "Denial-of-service in Django",
      "description": "If passed certain inputs, `django.utils.encoding.uri_to_iri` could lead to significant memory usage due to a recursion when repercent-encoding invalid UTF-8 octet sequences.",
      "cve": "Pipfile:Django:gemnasium:49afe3f9-e7ab-4d52-ad25-6e04bfdd670d",
      "severity": "High",
      "solution": "Upgrade to versions 1.11.23, 2.1.11, 2.2.4 or above.",
      "scanner": {
        "id": "gemnasium",
        "name": "Gemnasium"
      },
      "location": {
        "file": "Pipfile",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-49afe3f9-e7ab-4d52-ad25-6e04bfdd670d",
          "value": "49afe3f9-e7ab-4d52-ad25-6e04bfdd670d",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2019-14235.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2019-14235",
          "value": "CVE-2019-14235",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-14235"
        }
      ],
      "links": [
        {
          "url": "https://docs.djangoproject.com/en/dev/releases/security/"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-14235"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2019/aug/01/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "name": "Vulnerable Package",
          "type": "text",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "2bcb7f2de9ce4a4ed05c3dc8fcf52fb5a7d91a63e71e8a3b54bda390f4033b99",
      "category": "dependency_scanning",
      "name": "Improper Input Validation",
      "message": "Improper Input Validation in Django",
      "description": "Due to the behaviour of the underlying HTMLParser, `django.utils.html.strip_tags` would be extremely slow to evaluate certain inputs containing large sequences of nested incomplete HTML entities.",
      "cve": "Pipfile:Django:gemnasium:4be2dbd8-301a-48cd-8ffd-db2edf367ac4",
      "severity": "High",
      "solution": "Upgrade to versions 1.11.23, 2.1.11, 2.2.4 or above.",
      "scanner": {
        "id": "gemnasium",
        "name": "Gemnasium"
      },
      "location": {
        "file": "Pipfile",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-4be2dbd8-301a-48cd-8ffd-db2edf367ac4",
          "value": "4be2dbd8-301a-48cd-8ffd-db2edf367ac4",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2019-14233.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2019-14233",
          "value": "CVE-2019-14233",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-14233"
        }
      ],
      "links": [
        {
          "url": "https://docs.djangoproject.com/en/dev/releases/security/"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-14233"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2019/aug/01/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "name": "Vulnerable Package",
          "type": "text",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "8a4bf425d48ae09abb62a0757115ea48ac405e8f171b9ed16657b748b47f6e58",
      "category": "dependency_scanning",
      "name": "SQL Injection",
      "message": "SQL Injection in Django",
      "description": "Django allows SQL Injection if untrusted data is used as a tolerance parameter in GIS functions and aggregates on Oracle. By passing a suitably crafted tolerance to GIS functions and aggregates on Oracle, it was possible to break escaping and inject malicious SQL.",
      "cve": "Pipfile:Django:gemnasium:5295f830-9e63-4a53-a81c-c6a7745e7aa8",
      "severity": "High",
      "solution": "Upgrade to versions 1.11.29, 2.2.11, 3.0.4 or above.",
      "scanner": {
        "id": "gemnasium",
        "name": "Gemnasium"
      },
      "location": {
        "file": "Pipfile",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-5295f830-9e63-4a53-a81c-c6a7745e7aa8",
          "value": "5295f830-9e63-4a53-a81c-c6a7745e7aa8",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2020-9402.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2020-9402",
          "value": "CVE-2020-9402",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-9402"
        }
      ],
      "links": [
        {
          "url": "https://docs.djangoproject.com/en/3.0/releases/security/"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2020-9402"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2020/mar/04/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "name": "Vulnerable Package",
          "type": "text",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "94135d241c91ba2a854e18a7daf1aebb74f641b67c5954efba1cc1384fb8b271",
      "category": "dependency_scanning",
      "name": "Uncontrolled Memory Consumption",
      "message": "Uncontrolled Memory Consumption in Django",
      "description": "Django allows Uncontrolled Memory Consumption via a malicious attacker-supplied value to the `django.utils.numberformat.format()` function.",
      "cve": "Pipfile:Django:gemnasium:aa6b0729-ecca-4f48-8ea0-b364044c09cc",
      "severity": "High",
      "solution": "Upgrade to versions 1.11.20, 2.0.12, 2.1.7 or above.",
      "scanner": {
        "id": "gemnasium",
        "name": "Gemnasium"
      },
      "location": {
        "file": "Pipfile",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-aa6b0729-ecca-4f48-8ea0-b364044c09cc",
          "value": "aa6b0729-ecca-4f48-8ea0-b364044c09cc",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2019-6975.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2019-6975",
          "value": "CVE-2019-6975",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-6975"
        }
      ],
      "links": [
        {
          "url": "http://www.securityfocus.com/bid/106964"
        },
        {
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-6975"
        },
        {
          "url": "https://cwe.mitre.org/data/definitions/789.html"
        },
        {
          "url": "https://docs.djangoproject.com/en/dev/releases/security/"
        },
        {
          "url": "https://groups.google.com/forum/#!topic/django-announce/WTwEAprR0IQ"
        },
        {
          "url": "https://lists.fedoraproject.org/archives/list/package-announce@lists.fedoraproject.org/message/66WMXHGBXD7GSM3PEXVCMCAGLMQYHZCU/"
        },
        {
          "url": "https://lists.fedoraproject.org/archives/list/package-announce@lists.fedoraproject.org/message/HVXDOVCXLD74SHR2BENGCE2OOYYYWJHZ/"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-6975"
        },
        {
          "url": "https://usn.ubuntu.com/3890-1/"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2019/feb/11/security-releases/"
        },
        {
          "url": "https://www.openwall.com/lists/oss-security/2019/02/11/1"
        }
      ],
      "details": {
        "vulnerable_package": {
          "name": "Vulnerable Package",
          "type": "text",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "4742d91ba1f2c3ab12e0acc01a72d8507fa308c039332f6e4395ad3f01880ca9",
      "category": "dependency_scanning",
      "name": "Cross-site Scripting",
      "message": "Cross-site Scripting in Django",
      "description": "An issue was discovered in Django. The `clickable` Current URL value displayed by the `AdminURLFieldWidget` displays the provided value without validating it as a safe URL. Thus, an unvalidated value stored in the database, or a value provided as a URL query parameter payload, could result in a clickable JavaScript link.",
      "cve": "Pipfile:Django:gemnasium:149e1ab4-7152-4fa8-a40a-9a50a31d6d2f",
      "severity": "Medium",
      "solution": "Upgrade to versions 1.11.21, 2.1.9, 2.2.2 or above.",
      "scanner": {
        "id": "gemnasium",
        "name": "Gemnasium"
      },
      "location": {
        "file": "Pipfile",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-149e1ab4-7152-4fa8-a40a-9a50a31d6d2f",
          "value": "149e1ab4-7152-4fa8-a40a-9a50a31d6d2f",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2019-12308.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2019-12308",
          "value": "CVE-2019-12308",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-12308"
        }
      ],
      "links": [
        {
          "url": "https://docs.djangoproject.com/en/dev/releases/1.11.21/"
        },
        {
          "url": "https://docs.djangoproject.com/en/dev/releases/2.1.9/"
        },
        {
          "url": "https://docs.djangoproject.com/en/dev/releases/2.2.2/"
        },
        {
          "url": "https://docs.djangoproject.com/en/dev/releases/security/"
        },
        {
          "url": "https://github.com/django/django/commit/09186a13d975de6d049f8b3e05484f66b01ece62"
        },
        {
          "url": "https://github.com/django/django/commit/afddabf8428ddc89a332f7a78d0d21eaf2b5a673"
        },
        {
          "url": "https://github.com/django/django/commit/c238701859a52d584f349cce15d56c8e8137c52b"
        },
        {
          "url": "https://github.com/django/django/commit/deeba6d92006999fee9adfbd8be79bf0a59e8008"
        },
        {
          "url": "https://groups.google.com/forum/#!topic/django-announce/GEbHU7YoVz8"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-12308"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2019/jun/03/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "name": "Vulnerable Package",
          "type": "text",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "7047425f85c4b44a78bd2730b479060e36db7629c4f416fd5d147fc4125f4580",
      "category": "dependency_scanning",
      "name": "Incorrect Regular Expression",
      "message": "Incorrect Regular Expression in Django",
      "description": "If `django.utils.text.Truncator`'s `chars()` and `words()` methods were passed the `html=True` argument, they were extremely slow to evaluate certain inputs due to a catastrophic backtracking vulnerability in a regular expression. The `chars()` and words()` methods are used to implement the `truncatechars_html` and `truncatewords_html` template filters, which were thus vulnerable.",
      "cve": "Pipfile:Django:gemnasium:1f2c3ac1-b729-4ac9-8c64-1dcbff84250e",
      "severity": "Medium",
      "solution": "Upgrade to versions 1.8.19, 1.11.11, 2.0.3 or above.",
      "scanner": {
        "id": "gemnasium",
        "name": "Gemnasium"
      },
      "location": {
        "file": "Pipfile",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-1f2c3ac1-b729-4ac9-8c64-1dcbff84250e",
          "value": "1f2c3ac1-b729-4ac9-8c64-1dcbff84250e",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2018-7537.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2018-7537",
          "value": "CVE-2018-7537",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-7537"
        }
      ],
      "links": [
        {
          "url": "http://www.securityfocus.com/bid/103357"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2018-7537"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2018/mar/06/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "name": "Vulnerable Package",
          "type": "text",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "7b535e27ab1b95f76952c1b3b03d8d2716e010f360adab065b9598475873a309",
      "category": "dependency_scanning",
      "name": "URL Redirection to Untrusted Site (Open Redirect)",
      "message": "URL Redirection to Untrusted Site (Open Redirect) in Django",
      "description": "`django.middleware.common.CommonMiddleware` has an Open Redirect.",
      "cve": "Pipfile:Django:gemnasium:25669e1a-dcaa-4722-adfc-0f089945c95c",
      "severity": "Medium",
      "solution": "Upgrade to versions 1.11.15, 2.0.8 or above.",
      "scanner": {
        "id": "gemnasium",
        "name": "Gemnasium"
      },
      "location": {
        "file": "Pipfile",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-25669e1a-dcaa-4722-adfc-0f089945c95c",
          "value": "25669e1a-dcaa-4722-adfc-0f089945c95c",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2018-14574.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2018-14574",
          "value": "CVE-2018-14574",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-14574"
        }
      ],
      "links": [
        {
          "url": "http://www.securityfocus.com/bid/104970"
        },
        {
          "url": "http://www.securitytracker.com/id/1041403"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2018-14574"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2018/aug/01/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "name": "Vulnerable Package",
          "type": "text",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "b4d033d6a649416cb1ee225b78ccdfa307f41dd2579ac84912c57578ba12ccb2",
      "category": "dependency_scanning",
      "name": "Improper Input Validation",
      "message": "Improper Input Validation in Django",
      "description": "An HTTP request is not redirected to HTTPS when the `SECURE_PROXY_SSL_HEADER` and `SECURE_SSL_REDIRECT` settings are used, and the proxy connects to Django via HTTPS. In other words, `django.http.HttpRequest.scheme` has incorrect behavior when a client uses HTTP.",
      "cve": "Pipfile:Django:gemnasium:42367e41-2c04-4f04-bdaa-ea72d0957ef8",
      "severity": "Medium",
      "solution": "Upgrade to versions 1.11.22, 2.1.10, 2.2.3 or above.",
      "scanner": {
        "id": "gemnasium",
        "name": "Gemnasium"
      },
      "location": {
        "file": "Pipfile",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-42367e41-2c04-4f04-bdaa-ea72d0957ef8",
          "value": "42367e41-2c04-4f04-bdaa-ea72d0957ef8",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2019-12781.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2019-12781",
          "value": "CVE-2019-12781",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-12781"
        }
      ],
      "links": [
        {
          "url": "https://docs.djangoproject.com/en/dev/releases/security/"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-12781"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2019/jul/01/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "name": "Vulnerable Package",
          "type": "text",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "6cd9c5bd7b868dc9589affaacebc4e2dea4c9f7eaf43562f696a55ca813290a9",
      "category": "dependency_scanning",
      "name": "Incorrect Regular Expression",
      "message": "Incorrect Regular Expression in Django",
      "description": "An issue was discovered in Django. The `django.utils.html.urlize()` function was extremely slow to evaluate certain inputs due to catastrophic backtracking vulnerabilities in two regular expressions. The `urlize()` function is used to implement the urlize and urlizetrunc template filters, which were thus vulnerable.",
      "cve": "Pipfile:Django:gemnasium:483f1247-a680-44c4-b609-c7a3ac65bd82",
      "severity": "Medium",
      "solution": "Upgrade to versions 1.8.19, 1.11.11, 2.0.3 or above.",
      "scanner": {
        "id": "gemnasium",
        "name": "Gemnasium"
      },
      "location": {
        "file": "Pipfile",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-483f1247-a680-44c4-b609-c7a3ac65bd82",
          "value": "483f1247-a680-44c4-b609-c7a3ac65bd82",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2018-7536.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2018-7536",
          "value": "CVE-2018-7536",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-7536"
        }
      ],
      "links": [
        {
          "url": "http://www.securityfocus.com/bid/103361"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2018-7536"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2018/mar/06/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "name": "Vulnerable Package",
          "type": "text",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "806542ac5d43d8244d8ead95c7ce19a02d1d3e90da86fa906cc51d29834a46fd",
      "category": "dependency_scanning",
      "name": "Possible XSS in traceback section of technical 500 debug page",
      "message": "Possible XSS in traceback section of technical 500 debug page in Django",
      "description": "HTML auto-escaping was disabled in a portion of the template for the technical debug page. Given the right circumstances, this allowed a cross-site scripting attack. This vulnerability shouldn't affect most production sites since you shouldn't run with `DEBUG = True` (which makes this page accessible) in your production settings.",
      "cve": "Pipfile:Django:gemnasium:6162a015-8635-4a15-8d7c-dc9321db366f",
      "severity": "Medium",
      "solution": "Upgrade to versions 1.10.8, 1.11.5 or above.",
      "scanner": {
        "id": "gemnasium",
        "name": "Gemnasium"
      },
      "location": {
        "file": "Pipfile",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-6162a015-8635-4a15-8d7c-dc9321db366f",
          "value": "6162a015-8635-4a15-8d7c-dc9321db366f",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2017-12794.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2017-12794",
          "value": "CVE-2017-12794",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-12794"
        }
      ],
      "links": [
        {
          "url": "http://www.securityfocus.com/bid/100643"
        },
        {
          "url": "http://www.securitytracker.com/id/1039264"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2017-12794"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2017/sep/05/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "name": "Vulnerable Package",
          "type": "text",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "5bb5a7b9d71d02b4b0d2bb1ed2b59ee15d427ab3afef842b9d5d853a8c7814f1",
      "category": "dependency_scanning",
      "name": "Content Spoofing",
      "message": "Content Spoofing in Django",
      "description": "Improper Neutralization of Special Elements in Output Used by a Downstream Component issue exists in `django.views.defaults.page_not_found()`, leading to content spoofing (in a 404 error page) if a user fails to recognize that a crafted URL has malicious content.",
      "cve": "Pipfile:Django:gemnasium:94f5e552-ad49-49c7-bd9f-8857bba2354b",
      "severity": "Medium",
      "solution": "Upgrade to versions 1.11.18, 2.0.10, 2.1.5 or above.",
      "scanner": {
        "id": "gemnasium",
        "name": "Gemnasium"
      },
      "location": {
        "file": "Pipfile",
        "dependency": {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-94f5e552-ad49-49c7-bd9f-8857bba2354b",
          "value": "94f5e552-ad49-49c7-bd9f-8857bba2354b",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/Django/CVE-2019-3498.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2019-3498",
          "value": "CVE-2019-3498",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-3498"
        }
      ],
      "links": [
        {
          "url": "http://www.securityfocus.com/bid/106453"
        },
        {
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-3498"
        },
        {
          "url": "https://cwe.mitre.org/data/definitions/148.html"
        },
        {
          "url": "https://docs.djangoproject.com/en/dev/releases/security/"
        },
        {
          "url": "https://groups.google.com/forum/#!topic/django-announce/VYU7xQQTEPQ"
        },
        {
          "url": "https://lists.debian.org/debian-lts-announce/2019/01/msg00005.html"
        },
        {
          "url": "https://lists.fedoraproject.org/archives/list/package-announce@lists.fedoraproject.org/message/HVXDOVCXLD74SHR2BENGCE2OOYYYWJHZ/"
        },
        {
          "url": "https://nvd.nist.gov/vuln/detail/CVE-2019-3498"
        },
        {
          "url": "https://usn.ubuntu.com/3851-1/"
        },
        {
          "url": "https://www.debian.org/security/2019/dsa-4363"
        },
        {
          "url": "https://www.djangoproject.com/weblog/2019/jan/04/security-releases/"
        }
      ],
      "details": {
        "vulnerable_package": {
          "name": "Vulnerable Package",
          "type": "text",
          "value": "Django:1.11.3"
        }
      }
    },
    {
      "id": "0e3e740befa34d452fcb176974a90c219292068c354069c0a3e97acf0f3d01a6",
      "category": "dependency_scanning",
      "name": "Session fixation in resolve_redirects()",
      "message": "Session fixation in resolve_redirects() in requests",
      "description": "The `resolve_redirects()` function in `sessions.py` allows a remote, user-assisted attacker to conduct a session fixation attack. This flaw exists because the application, when establishing a new session, does not invalidate an existing session identifier and assign a new one. With a specially crafted request fixating the session identifier, a context-dependent attacker can ensure a user authenticates with the known session identifier, allowing the session to be subsequently hijacked.",
      "cve": "Pipfile:requests:gemnasium:d1c3b3de-3b82-4f45-97ce-7e97f96652e5",
      "severity": "Medium",
      "solution": "Upgrade to latest version.",
      "scanner": {
        "id": "gemnasium",
        "name": "Gemnasium"
      },
      "location": {
        "file": "Pipfile",
        "dependency": {
          "package": {
            "name": "requests"
          },
          "version": "2.5.3"
        }
      },
      "identifiers": [
        {
          "type": "gemnasium",
          "name": "Gemnasium-d1c3b3de-3b82-4f45-97ce-7e97f96652e5",
          "value": "d1c3b3de-3b82-4f45-97ce-7e97f96652e5",
          "url": "https://gitlab.com/gitlab-org/security-products/gemnasium-db/-/blob/v1.2.142/pypi/requests/CVE-2015-2296.yml"
        },
        {
          "type": "cve",
          "name": "CVE-2015-2296",
          "value": "CVE-2015-2296",
          "url": "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-2296"
        }
      ],
      "links": [
        {
          "url": "http://osvdb.org/show/osvdb/119576"
        },
        {
          "url": "http://www.openwall.com/lists/oss-security/2015/03/14/4"
        },
        {
          "url": "https://github.com/kennethreitz/requests/commit/3bd8afbff29e50b38f889b2f688785a669b9aafc#diff-28e67177469c0d36b068d68d9f6043bf"
        },
        {
          "url": "https://github.com/kennethreitz/requests/commit/f7c85685a8e484715649c13bacae6adc7f5f3908#diff-28e67177469c0d36b068d68d9f6043bf"
        }
      ],
      "details": {
        "vulnerable_package": {
          "name": "Vulnerable Package",
          "type": "text",
          "value": "requests:2.5.3"
        }
      }
    }
  ],
  "dependency_files": [
    {
      "path": "Pipfile",
      "package_manager": "pipenv",
      "dependencies": [
        {
          "package": {
            "name": "Django"
          },
          "version": "1.11.3"
        },
        {
          "package": {
            "name": "docutils"
          },
          "version": "0.13.1"
        },
        {
          "package": {
            "name": "pytz"
          },
          "version": "2022.2.1"
        },
        {
          "package": {
            "name": "requests"
          },
          "version": "2.5.3"
        }
      ]
    }
  ],
  "scan": {
    "analyzer": {
      "id": "gemnasium-python",
      "name": "gemnasium-python",
      "url": "https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium",
      "vendor": {
        "name": "GitLab"
      },
      "version": "3.6.0"
    },
    "scanner": {
      "id": "gemnasium-python",
      "name": "gemnasium-python",
      "url": "https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium",
      "vendor": {
        "name": "GitLab"
      },
      "version": "2.19.3"
    },
    "type": "dependency_scanning",
    "start_time": "2022-02-17T15:56:49",
    "end_time": "2022-02-17T15:57:05",
    "status": "success"
  }
}
