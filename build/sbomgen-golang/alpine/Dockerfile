FROM golang:1.19-alpine AS sbomgen-build

ENV CGO_ENABLED=0

WORKDIR /go/src/app
COPY . .

# build the sbomgen binary and automatically set the Version
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
    go build -ldflags="-X 'main.Version=$CHANGELOG_VERSION'" -o /sbomgen-golang \
    cmd/sbomgen-golang/main.go

FROM alpine:3.16

# set user HOME to a directory where any user can write (OpenShift)
ENV HOME "/tmp"

COPY --from=sbomgen-build /sbomgen-golang /usr/local/bin/sbomgen-golang

ENTRYPOINT []
CMD ["sbomgen-golang", "run"]
